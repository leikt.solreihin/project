# Project

This repository is aimed to be a replicable version of Pokémon SDK. It helps contributor to mount a project quickly and start working over the several git repostory composing the whole PSDK thing.

## How to mount the project

First of all, make sure you have setup SSH on your gitlab account, this is mandatory to work with PSDK. After that, make sure you went to [Pokémon Workshop Discord](https://discord.gg/0noB0gBDd91B8pMk) and asked permission to download Pokémon SDK.

Once you're ready with the step above, you can follow the following steps:
1. Clone the project: `git clone git@gitlab.com:pokemonsdk/project.git`
2. Go inside the project `cd project`
3. Pull the submodules: `git submodule update --init --recursive`
4. On Windows
   1. Download all the archives from the channel `#git-deps` into the `project` folder.
   2. Extract them (extract here) with 7-zip
5. On Linux
   1. https://149366088.v2.pressablecdn.com/wp-content/uploads/2018/08/linux-birthday-cake-joke-750x393.jpg
   2. Seriously, instructions will come latter but what you need to do is installing Ruby 3.0, SFML 2.5, Building LiteRGSS2, Building RubyFmod etc...
6. On MacOS
   1. What a terrible choice (it's a joke), regardless, you can start by using this: https://brew.sh
   2. And guess how to build LiteRGSS2 for MacOS
   3. `[Optional]` You can buy Nuri Yuri a Mac so (s)he might be able to make a proper build of this.
7. Run the command: `game --util=restore` (replace game with `ruby game.rb` for non Windows OS)
8. Run the command: `game --util=compile_text` (replace game with `ruby game.rb` for non Windows OS)
   1. Choose `en,fr,es`

Note: You can alternatively use Wine since for now we still depends on RMXP which requires Wine to run (afaik).

## How to contribute

### Updating the data

If want to update the data (eg. Pokemon, Item, Move, ...)
1. Pull the latest version of project
2. Create a new branch
3. Run the command `game --util=restore` (replace game with `ruby game.rb` for non Windows OS)
4. Edit the data using Ruby Host*
5. Run the command `game --util=convert` (replace game with `ruby game.rb` for non Windows OS)
6. Push your changes
7. Create a new Merge Request

### Updating the texts

If you want to update the texts
1. Pull the latest version of project
2. Create a new branch
3. Delete all the `.dat` files in `Data/Text/Dialogs`
4. Update the `.csv` files you need to update in `Data/Text/Dialogs`
5. Run the command: `game --util=compile_text` (replace game with `ruby game.rb` for non Windows OS)
6. Run the game to see if the text got updated
7. Push your changes
8. Create a new Merge Request

### Updating the graphics

This process might be done through Discord!

### Updating the audio

This process might be done through Discord!

### Updating the dependencies

This process might be done through Discord!

